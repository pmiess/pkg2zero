#!/usr/bin/env python
# Copyright (C) 2009, Thomas Leonard
# Copyright (C) 2008, Anders F Bjorklund
# See the COPYING file for details, or visit http://0install.net.

import sys, time
from optparse import OptionParser
import tempfile, shutil, os
from xml.dom import minidom
import gzip
from zeroinstall.injector.versions import format_version
try:
	import xml.etree.cElementTree as ET # Python 2.5
except ImportError:
	try:
		import xml.etree.ElementTree as ET
	except ImportError:
		try:
			import cElementTree as ET # http://effbot.org
		except ImportError:
			import elementtree.ElementTree as ET

import subprocess
try:
	from subprocess import check_call
except ImportError:
	def check_call(*popenargs, **kwargs):
		rc = subprocess.call(*popenargs, **kwargs)
		if rc != 0: raise OSError, rc

from zeroinstall.injector import model, qdom, distro
from zeroinstall.zerostore import unpack

from support import read_child, add_node, Mappings

manifest_algorithm = 'sha1new'

deb_category_to_freedesktop = {
	'devel' : 'Development',
	'web' : 'Network',
	'graphics' : 'Graphics',
	'games' : 'Game',
}

rpm_group_to_freedesktop = {
	'Development/Libraries' : 'Development',
}

valid_categories = [
	'AudioVideo',
	'Audio',
	'Video',
	'Development',
	'Education',
	'Game',
	'Graphics',
	'Network',
	'Office',
	'Settings',
	'System',
	'Utility',
]

# Parse command-line arguments

parser = OptionParser('usage: %prog [options] http://.../package.deb [target-feed.xml]\n'
		      '       %prog [options] http://.../package.rpm [target-feed.xml]\n'
		      '       %prog [options] http://.../package.xz [target-feed.xml]\n'
		      '       %prog [options] package-name [target-feed.xml]\n'
		      'Publish a Debian or RPM package in a Zero Install feed.\n'
		      "target-feed.xml is created if it doesn't already exist.")
parser.add_option("-a", "--archive-url", help="archive to use as the package contents")
parser.add_option("", "--archive-extract", help="only extract files under this subdirectory")
parser.add_option("", "--license", help="value for 'license' attribute")
parser.add_option("-r", "--repomd-file", help="repository metadata file")
parser.add_option("", "--path", help="location of packages [5/os/i386]")
parser.add_option("-p", "--packages-file", help="Debian package index file")
parser.add_option("-d", "--database-file", help="pacman datebase archive")
parser.add_option("-m", "--mirror", help="location of packages [http://ftp.debian.org/debian] or [http://mirror.centos.org/centos]")
parser.add_option("-k", "--key", help="key to use for signing")
(options, args) = parser.parse_args()

if len(args) < 1 or len(args) > 2:
	parser.print_help()
	sys.exit(1)

# Load dependency mappings
mappings = Mappings()

class Package:
	name = '(unknown)'
	version = None
	arch = None
	category = None
	homepage = None
	buildtime = None
	license = None

	def __init__(self):
		self.requires = []

class DebRepo:
	def __init__(self, options):
		self.packages_base_url = (options.mirror or 'http://ftp.debian.org/debian') + '/'
		self.packages_file = options.packages_file or 'Packages'

	def get_repo_metadata(self, pkg_name):
		if not os.path.isfile(self.packages_file):
			print >>sys.stderr, ("File '%s' not found (use -p to give its location).\n"
				"Either download one (e.g. ftp://ftp.debian.org/debian/dists/stable/main/binary-amd64/Packages.bz2),\n"
				"or specify the full URL of the .deb package to use.") % self.packages_file
			sys.exit(1)
		if self.packages_file.endswith('.bz2'):
			import bz2
			opener = bz2.BZ2File
		else:
			opener = file
		pkg_data = "\n" + opener(self.packages_file).read()
		try:
			i = pkg_data.index('\nPackage: %s\n' % pkg_name)
		except ValueError:
			raise Exception("Package '%s' not found in Packages file '%s'." % (pkg_name, self.packages_file))
		j = pkg_data.find('\n\n', i)
		if j == -1:
			pkg_info = pkg_data[i:]
		else:
			pkg_info = pkg_data[i:j]
		filename = None
		digest = {}
		for line in pkg_info.split('\n'):
			if ':' in line and not line.startswith(' '):
				key, value = line.split(':', 1)
				if key == 'Filename':
					filename = value.strip()
				elif key in ('SHA1', 'SHA256'):
					digest[key.lower()] = value.strip()
		if filename is None:
			raise Exception('Filename: field not found in package data:\n' + pkg_info)
		pkg_url = self.packages_base_url + filename

		return pkg_url, digest

	def get_package_metadata(self, pkg_file):
		package = Package()

		details = read_child(['dpkg-deb', '--info', pkg_file])

		description_and_summary = details.split('\n Description: ')[1].split('\n')
		package.summary = description_and_summary[0]
		description = ''
		for x in description_and_summary[1:]:
			if not x: continue
			assert x[0] == ' '
			x = x[1:]
			if x[0] != ' ':
				break
			if x == ' .':
				description += '\n'
			else:
				description += x[1:].replace('.  ', '. ') + '\n'
		package.description = description.strip()

		for line in details.split('\n'):
			if not line: continue
			assert line.startswith(' ')
			line = line[1:]
			if ':' in line:
				key, value = line.split(':', 1)
				value = value.strip()
				if key == 'Section':
					package.category = deb_category_to_freedesktop.get(value)
					if not package.category:
						if value != 'libs':
							print >>sys.stderr, "Warning: no mapping for Debian category '%s'" % value
				elif key == 'Package':
					package.name = value
				elif key == 'Version':
					value = value.replace('cvs', '')
					value = value.replace('svn', '')
					if ':' in value: value = value.split(':', 1)[1]
					package.version = distro.try_cleanup_distro_version(value)
				elif key == 'Architecture':
					if '-' in value:
						arch, value = value.split('-', 1)
					else:
						arch = 'linux'
					if value == 'amd64':
						value = 'x86_64'
					elif value == 'all':
						value = '*'
					package.arch = arch.capitalize() + '-' + value
				elif key == 'Depends':
					for x in value.split(','):
						req = mappings.process(x)
						if req:
							package.requires.append(req)
		return package

class RPMRepo:
	def __init__(self, options):
		self.packages_base_url = (options.mirror or 'http://mirror.centos.org/centos') + '/'
		self.packages_base_dir = (options.path or '5/os/i386') + '/'
		self.repomd_file = options.repomd_file or 'repodata/repomd.xml'
		if not os.path.isfile(self.repomd_file):
			print >>sys.stderr, ("File '%s' not found (use -r to give its location).\n"
				"Either download one (e.g. http://mirror.centos.org/centos/5/os/i386/repodata/repomd.xml),\n"
				"or specify the full URL of the .rpm package to use.") % self.repomd_file
			sys.exit(1)

	def get_repo_metadata(self, pkg_name):
		primary_file = None
		repomd = minidom.parse(self.repomd_file)
		repo_top = os.path.dirname(os.path.dirname(self.repomd_file))
		for data in repomd.getElementsByTagName("data"):
			if data.attributes["type"].nodeValue == "primary":
				for node in data.getElementsByTagName("location"):
					primary_file = os.path.join(repo_top, node.attributes["href"].nodeValue)
		location = None
		primary = ET.parse(gzip.open(primary_file))
		NS = "http://linux.duke.edu/metadata/common"
		metadata = primary.getroot()
		pkg_data = None
		for package in metadata.findall("{%s}package" % NS):
			if package.find("{%s}name" % NS).text == pkg_name:
				pkg_data = package
				location = pkg_data.find("{%s}location" % NS).get("href")
				break
		if pkg_data is None:
			raise Exception("Package '%s' not found in repodata." % pkg_name)
		checksum = pkg_data.find("{%s}checksum" % NS)
		digest = {}
		if checksum.get("type") == "sha":
			digest["sha1"] = checksum.text
		if checksum.get("type") == "sha256":
			digest["sha256"] = checksum.text
		if location is None:
			raise Exception('location tag not found in primary metadata:\n' + primary_file)
		pkg_url = self.packages_base_url + self.packages_base_dir + location

		return pkg_url, digest

	def get_package_metadata(self, pkg_file):
		package = Package()

		query_format = '%{SUMMARY}\\a%{DESCRIPTION}\\a%{NAME}\\a%{VERSION}\\a%{OS}\\a%{ARCH}\\a%{URL}\\a%{GROUP}\\a%{LICENSE}\\a%{BUILDTIME}\\a[%{REQUIRES}\\n]'
		headers = read_child(['rpm', '--qf', query_format, '-qp', pkg_file]).split('\a')

		package.summary = headers[0].strip()
		package.description = headers[1].strip()

		package.name = headers[2]
		value = headers[3]
		value = value.replace('cvs', '')
		value = value.replace('svn', '')
		value = distro.try_cleanup_distro_version(value)
		package.version = value
		value = headers[4]
		package.arch = value.capitalize()
		value = headers[5]
		if value == 'amd64':
			value = 'x86_64'
		if value == 'noarch':
			value = '*'
		package.arch += '-' + value
		value = headers[6].strip()
		package.page = value
		category = None
		value = headers[7].strip()
		package.category = rpm_group_to_freedesktop.get(value)
		if not category:
			print >>sys.stderr, "Warning: no mapping for RPM group '%s'" % value

		value = headers[8].strip()
		package.license = value
		value = headers[9].strip()
		package.buildtime = long(value)
		value = headers[10].strip()
		for x in value.split('\n'):
			if x.startswith('rpmlib'):
				continue
			req = mappings.process(x)
			if req:
				package.requires.append(req)
		return package

class PacRepo(object):
    """ An arch pacman repo """
    def __init__(self, myoptions):
	self.packages_base_url = (myoptions.mirror or 'http://repo.msys2.org/mingw/x86_64') + '/'
	self.database_file = myoptions.database_file or 'mingw64.db.tar.gz'

    def get_repo_metadata(self, pkg_name):
	""" Retrive metadata for pkg_name from the pacman repo """
	if not os.path.isfile(self.database_file):
		print >>sys.stderr, ("File '%s' not found (use -p to give its location).\n"
				 "Either download one (e.g. http://repo.msys2.org/mingw/x86_64/ming"
				 "w64.db.tar.gz ),\nor specify the full URL of the pacman package t"
				 "o use.") % self.database_file
		sys.exit(1)
	import tarfile
	tar = tarfile.open(self.database_file)
	member = tar.next()
	while member != None:
		if member.name.startswith(pkg_name):
			member = tar.next()
			break
		member = tar.next()

	if member is None:
		raise Exception("Package '%s' not found in Packages file '%s'."
				% (pkg_name, self.database_file))

	descfile = tar.extractfile(member)
	if descfile is not None:
		pkg_info = descfile.read()

	filename = None
	packagedigest = {}
	lines = pkg_info.splitlines()
	if lines[0] != '%FILENAME%':
		raise Exception('first line shold be %FILENAME%')
	filename = lines[1]

	if '%MD5SUM%' in lines:
		packagedigest['MD5'] = lines[lines.index('%MD5SUM%')+1]
	if '%SHA1SUM%' in lines:
		packagedigest['sha1'] = lines[lines.index('%SHA1SUM%')+1]
	if '%SHA256SUM%' in lines:
		packagedigest['sha256'] = lines[lines.index('%SHA256SUM%')+1]
	if '%SHA384SUM%' in lines:
		packagedigest['sha384'] = lines[lines.index('%SHA384SUM%')+1]
	if '%SHA512SUM%' in lines:
		packagedigest['sha512'] = lines[lines.index('%SHA512SUM%')+1]

	if filename is None:
		raise Exception('Filename: field not found in package data:\n' + pkg_info)

	pkg_url = self.packages_base_url + filename

	return pkg_url, packagedigest

    @staticmethod
    def _get_package_info(apkg_file):
	""" Extract the .PKGINFO from the packagefile """
	import contextlib
	import backports.lzma
	import tarfile

	pkginfo = None
	if not apkg_file.endswith('xz'):
		raise Exception("package file not in tar.xz format")

	with contextlib.closing(backports.lzma.LZMAFile(apkg_file)) as xzfile:
		with tarfile.open(fileobj=xzfile) as atarfile:
		member = atarfile.next()
		while member != None:
			if member.name.startswith('.PKGINFO'):
			break
			member = atarfile.next()
		pkginfofile = atarfile.extractfile(member)
		if pkginfofile is None:
			raise Exception("pkginfo not found")
		pkginfo = pkginfofile.read()

	return pkginfo

	@staticmethod
	def _pacman_arch_to_0install_arch(value, package_name):
	""" convert a arch from a pacman package to a 0install arch """

	if '-' in value:
		arch, value = value.split('-', 1)
	else:
		arch = 'windows'

	if value == 'amd64':
		value = 'x86_64'
	elif package_name.startswith('mingw-w64-x86_64'):
		value = 'x86_64'
		arch = 'windows'
	elif package_name.startswith('mingw-w64-i686'):
		value = 'i686'
		arch = 'windows'
	elif value == 'any':
		value = '*'
	return arch.capitalize() + '-' + value

	@staticmethod
	def _add_key_to_package(key, value, apackage):
	""" add the key the the package """

	if key == 'pkgname':
		apackage.name = value
	elif key == 'pkgver':
		value = value.replace('cvs', '')
		value = value.replace('svn', '')
		value = value.replace('git', '')
		value = value.replace('hg', '')
		apackage.version = distro.try_cleanup_distro_version(value)
	elif key == 'pkgdesc':
		apackage.summary = value
	elif key == 'url':
		apackage.homepage = value
	elif key == 'builddate':
		apackage.buildtime = long(value)
	elif key == 'arch':
		apackage.arch = PacRepo._pacman_arch_to_0install_arch(value, apackage.name)
	elif key == 'license':
		if apackage.license is None:
		apackage.license = value
		else:
		apackage.license += ', '
		apackage.license += value
	elif key == 'depend':
		dep = mappings.process(value)
		if dep:
		apackage.requires.append(dep)
	return apackage

    @staticmethod
    def get_package_metadata(apkg_file):
	""" Retrive metadate from pkg_file """
	apackage = Package()
	apackage.description = ""
	
	pkginfo = PacRepo._get_package_info(apkg_file)

	for currentline in pkginfo.split('\n'):
		if not currentline or currentline.startswith('#'):
		continue
		if '=' in currentline:
		key, value = currentline.split('=', 1)
		key = key.strip()
		value = value.strip()
		apackage = PacRepo._add_key_to_package(key, value, apackage)
	return apackage


if args[0].endswith('.deb') or options.packages_file:
    repo = DebRepo(options)
elif args[0].endswith('.rpm') or options.repomd_file:
    repo = RPMRepo(options)
elif args[0].endswith('.xz') or options.database_file:
    repo = PacRepo(options)
else:
    print >>sys.stderr, "Use --packages-file for Debian, --repomd-file for RPM, or --datebase-file for Pacman"
    sys.exit(1)

pkg_data = None

if options.archive_url:
	pkg_file = os.path.abspath(args[0])
	archive_url = options.archive_url
	archive_file = os.path.abspath(archive_url.rsplit('/', 1)[1])
	digest = {}
	assert os.path.exists(pkg_file), ("%s doesn't exist!" % pkg_file)
else:
	scheme = args[0].split(':', 1)[0]
	if scheme in ('http', 'https', 'ftp'):
		archive_url = args[0]
		digest = {}
	else:
		archive_url, digest = repo.get_repo_metadata(args[0])
	archive_file = pkg_file = os.path.abspath(archive_url.rsplit('/', 1)[1])

# pkg_url, pkg_archive = .deb or .rpm with the metadata
# archive_url, archive_file = .dep, .rpm or .tar.bz2 with the contents
#
# Often pkg == archive, but sometimes it's useful to convert packages to tarballs
# so people don't need special tools to extract them.


# Download package, if required

if not os.path.exists(pkg_file):
	print >>sys.stderr, "File '%s' not found, so downloading from %s..." % (pkg_file, archive_url)
	file_part = pkg_file + '.part'
	check_call(['wget', '-O', file_part, archive_url])
	os.rename(file_part, pkg_file)

# Check digest, if known

if "sha256" in digest:
	import hashlib
	m = hashlib.new('sha256')
	expected_digest = digest["sha256"]
elif "sha1" in digest:
	try:
		import hashlib
		m = hashlib.new('sha1')
	except ImportError:
		import sha
		m = sha.new()
	expected_digest = digest["sha1"]
else:
	m = None

if m:
	m.update(file(archive_file).read())
	actual = m.hexdigest()
	if actual != expected_digest:
		raise Exception("Incorrect digest on package file! Was " + actual + ", but expected " + expected_digest)
	else:
		print "Package's digest matches value in reposistory metadata (" + actual + "). Good."
else:
	print >>sys.stderr, "Note: no SHA-1 or SHA-256 digest known for this package, so not checking..."

# Extract meta-data from package

pkg_metadata = repo.get_package_metadata(pkg_file)

# Unpack package, find binaries and .desktop files, and add to cache

possible_commands = []
bash_commands = []
posix_commands = []
perl_commands = []
python_commands = []

icondata = None
tmp = tempfile.mkdtemp(prefix = 'pkg2zero-')
try:
	unpack_dir = tmp
	unpack.unpack_archive(archive_file, open(archive_file), destdir = unpack_dir, extract = options.archive_extract)
	if options.archive_extract:
		unpack_dir = os.path.join(unpack_dir, options.archive_extract)

	icon = None
	images = {}
	for root, dirs, files in os.walk(unpack_dir):
		assert root.startswith(unpack_dir)
		relative_root = root[len(unpack_dir) + 1:]
		for name in files:
			full = os.path.join(root, name)
			import magic

			f = os.path.join(relative_root, name)
			ident = f, magic.from_file(full)
 
			if 'executable' in ident[1]
				if 'Bourne-Again shell script' in ident[1]
					bash_commands.append(f)
				if 'POSIX shell script' in ident[1]
					posix_commands.append(f)
				if 'Perl script' in ident[1]
					perl_commands.append(f)
				if 'Python script' in ident[1]
					python_commands.append(f)

			if f.endswith('.desktop'):
				for line in file(full):
					if line.startswith('Categories'):
						for cat in line.split('=', 1)[1].split(';'):
							cat = cat.strip()
							if cat in valid_categories:
								category = cat
								break
					elif line.startswith('Icon'):
						icon = line.split('=', 1)[1].strip()
			elif f.startswith('bin/') or f.startswith('usr/bin/') or f.startswith('usr/games/'):
				if os.path.isfile(full) and not f.endswith('.dll'):
					possible_commands.append(f)
			elif f.endswith('.png') or f.endswith('.ico'):
				images[f] = full
				images[os.path.basename(f)] = full
				# make sure to also map basename without the extension
				images[os.path.splitext(os.path.basename(f))[0]] = full

	icondata = None
	if icon in images:
		print "Using %s for icon" % os.path.basename(images[icon])
		icondata = file(images[icon]).read()
	
	manifest = read_child(['0store', 'manifest', unpack_dir, manifest_algorithm])
	digest = manifest.rsplit('\n', 2)[1]
	check_call(['0store', 'add', digest, unpack_dir])
finally:
	shutil.rmtree(tmp)

if possible_commands:
	sorted_possible_commands = sorted(possible_commands, key = len)
	
	pkg_main = sorted_possible_commands[0]
	if len(possible_commands) > 1:
		print "Warning: several possible main binaries found:"
		print "- " + pkg_main + " (I chose this one)"
		for x in sorted_possible_commands[1:]:
			print "- " + x
else:
	pkg_main = None

# Make sure we haven't added this version already...

if len(args) > 1:
	target_feed_file = args[1]
	target_icon_file = args[1].replace('.xml', '.png')
else:
	target_feed_file = pkg_metadata.name + '.xml'
	target_icon_file = pkg_metadata.name + '.png'

feed_uri = None
icon_uri = None
if os.path.isfile(target_feed_file):
	dom = qdom.parse(file(target_feed_file))
	old_target_feed = model.ZeroInstallFeed(dom, local_path = target_feed_file)
	existing_impl = old_target_feed.implementations.get(digest)
	if existing_impl:
		print >>sys.stderr, ("Feed '%s' already contains an implementation with this digest!\n%s" % (target_feed_file, existing_impl))
		sys.exit(1)
else:
	# No target, so need to pick a URI
	feed_uri = mappings.lookup(pkg_metadata.name)
	if feed_uri is None:
		suggestion = mappings.get_suggestion(pkg_metadata.name)
		uri = raw_input('Enter the URI for this feed [%s]: ' % suggestion).strip()
		if not uri:
			uri = suggestion
		assert uri.startswith('http://') or uri.startswith('https://') or uri.startswith('ftp://'), uri
		feed_uri = uri
		mappings.add_mapping(pkg_metadata.name, uri)

	if icondata and not os.path.isfile(target_icon_file):
		file = open(target_icon_file, 'wb')
		file.write(icondata)
		file.close()
	if icon_uri is None:
		suggestion = 'http://0install.net/feed_icons/' + target_icon_file
		uri = raw_input('Enter the URI for this icon [%s]: ' % suggestion).strip()
		if not uri:
			uri = suggestion
		assert uri.startswith('http://') or uri.startswith('https://') or uri.startswith('ftp://'), uri
		icon_uri = uri

# Create a local feed with just the new version...

template = '''<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://zero-install.sourceforge.net/2004/injector/interface http://0install.de/schema/injector/interface/interface.xsd http://0install.de/schema/desktop-integration/capabilities http://0install.de/schema/desktop-integration/capabilities/capabilities.xsd">
</interface>'''
doc = minidom.parseString(template)
root = doc.documentElement

add_node(root, 'name', pkg_metadata.name)
add_node(root, 'summary', pkg_metadata.summary)
add_node(root, 'description', pkg_metadata.description)
feed_for = add_node(root, 'feed-for', '')
if feed_uri:
	feed_for.setAttribute('interface', feed_uri)
if icon_uri:
	icon = add_node(root, 'icon')
	icon.setAttribute('href', icon_uri)
	icon.setAttribute('type', 'image/png')
if pkg_metadata.homepage:
	add_node(root, 'homepage', pkg_metadata.homepage)
if pkg_metadata.category:
	add_node(root, 'category', pkg_metadata.category)

package = add_node(root, 'package-implementation', '')
package.setAttribute('package', pkg_metadata.name)

group = add_node(root, 'group', '')



if pkg_metadata.arch:
	group.setAttribute('arch', pkg_metadata.arch)
else:
	print >>sys.stderr, "No Architecture: field in package"
if pkg_metadata.license:
	group.setAttribute('license', pkg_metadata.license)

for req in pkg_metadata.requires:
	req_element = add_node(group, 'requires', before = '\n    ', after = '')
	req_element.setAttribute('interface', req.interface)
	if req.restrictions :
		req_element.setAttribute('version', format_version(req.restrictions[0].version))
	binding = add_node(req_element, 'environment', before = '\n      ', after = '\n    ')
	binding.setAttribute('name', 'LD_LIBRARY_PATH')
	binding.setAttribute('insert', 'usr/lib')
	if pkg_metadata.arch.startswith('Windows-'):
		bindingpath = add_node(req_element, 'environment', before = '\n      ', after = '\n    ')
		bindingpath.setAttribute('name', 'PATH')
		bindingpath.setAttribute('insert', 'bin')

for x in possible_commands:
	command = add_node( group, 'command', before = '\n    ', after = '\n  ')
	if pkg_main is x:
		command.setAttribute('name', 'run')
	else:
		command.setAttribute('name', os.path.basename(os.path.splitext(x)[0]))
	command.setAttribute('path', x)
	if x.endswith('.py') or x in python_commands :
		runner = add_node( command, 'runner', before = '\n    ', after = '\n  ')
		runner.setAttribute('interface', 'http://repo.roscidus.com/python/python')
	elif x.endswith('.tcl'):
		runner = add_node( command, 'runner', before = '\n    ', after = '\n  ')
		runner.setAttribute('interface', 'http://repo.roscidus.com/tcl/tcl')
	elif x.endswith('.pl') or x in perl_commands :
		runner = add_node( command, 'runner', before = '\n    ', after = '\n  ')
		runner.setAttribute('interface', 'http://repo.roscidus.com/perl/perl')
	elif x.endswith('.sh') or x in posix_commands :
		runner = add_node( command, 'runner', before = '\n    ', after = '\n  ')
		runner.setAttribute('interface', 'http://repo.roscidus.com/bash')
		runarg = add_node( runner, 'arg', '--posix', before = '\n     ', after = '\n    ')
	elif x.endswith('.ps1'):
		runner = add_node( command, 'runner', before = '\n    ', after = '\n  ')
		runner.setAttribute('http://repo.roscidus.com/powershell/powershell')
	elif x.endswith('.bash') or x in bash_commands :
		runner = add_node( command, 'runner', before = '\n    ', after = '\n  ')
		runner.setAttribute('interface', 'http://repo.roscidus.com/bash')
	elif x.endswith('.awk') :
		runner = add_node( command, 'runner', before = '\n    ', after = '\n  ')
		runner.setAttribute('interface', 'http://repo.roscidus.com/awk/awk')
		


impl = add_node(group, 'implementation', before = '\n    ', after = '\n  ')
impl.setAttribute('id', digest)
assert pkg_metadata.version
impl.setAttribute('version', pkg_metadata.version)

if options.license:
	impl.setAttribute('license', options.license)

if pkg_metadata.buildtime:
	impl.setAttribute('released', time.strftime('%Y-%m-%d', time.localtime(pkg_metadata.buildtime)))
else:
	impl.setAttribute('released', time.strftime('%Y-%m-%d'))

archive = add_node(impl, 'archive', before = '\n      ', after = '\n    ')
archive.setAttribute('href', archive_url)
archive.setAttribute('size', str(os.path.getsize(archive_file)))
if options.archive_extract:
	archive.setAttribute('extract', options.archive_extract)


for x in possible_commands:
	entrypoint = add_node( root, 'entry-point', before = '\n    ', after = '\n  ')
	name = os.path.basename(os.path.splitext(x)[0])
	if pkg_main is x:
		entrypoint.setAttribute('command', 'run')
	else:
		entrypoint.setAttribute('command', name)
	entrypoint.setAttribute('binary-name', name)

# Add our new version to the main feed...

output_stream = tempfile.NamedTemporaryFile(prefix = 'pkg2zero-')
try:
	output_stream.write("<?xml version='1.0'?>\n")
	root.writexml(output_stream)
	output_stream.write('\n')
	output_stream.flush()

	publishing_options = []
	if options.key:
		# Note: 0publish < 0.16 requires the --xmlsign option too
		publishing_options += ['--xmlsign', '--key', options.key]
	check_call([os.environ['PUBLISH_COMMAND']] + publishing_options + ['--local', output_stream.name, target_feed_file])
	print "Added version %s to %s" % (pkg_metadata.version, target_feed_file)
finally:
	output_stream.close()
